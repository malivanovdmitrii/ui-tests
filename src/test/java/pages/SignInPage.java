package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SignInPage {

    public static final SelenideElement
            SIGN_IN_TITLE_TEXT = $(By.xpath("//*[@class='signup__title-form signup__title-form--sign-in']"));

    public static final String
            SIGN_IN_PAGE_URL = "/login/";
}
