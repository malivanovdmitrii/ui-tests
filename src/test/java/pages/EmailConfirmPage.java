package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EmailConfirmPage {

    public static final SelenideElement
            EMAIL_CONFIRM_MIRO_LOGO = $(By.xpath("//*[@title='Miro Logo']")),
            EMAIL_CONFIRM_TITLE_TEXT = $(".signup__title-form"),
            EMAIL_CONFIRM_SUBTITLE_TEXT = $(".signup__subtitle-form"),
            EMAIL_CONFIRM_CODE_FIELD = $(By.id("code")),
            EMAIL_CONFIRM_CODE_FIELD_PlACEHOLDER = $(".sr-only"),
            EMAIL_CONFIRM_FOOTER_TEXT = $(By.xpath("//*[@class='signup__footer']")),
            EMAIL_CONFIRM_RESENT_CODE_LINK = $(By.xpath("//*[@href='/get-new-code/']")),
            EMAIL_CONFIRM_HELP_CENTER_LINK = $(By.xpath("//a[contains(@href, 'help.miro.com')]"));


    public static final String
            EMAIL_CONFIRM_URL = "/email-confirm/";
}
