package steps;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.webdriver;
import static com.codeborne.selenide.WebDriverConditions.url;

public class BaseSteps {

    @Step
    public void openPage(String url) {
        open(url);
    }

    @Step
    public void clickOnElement(SelenideElement element) {
        element.shouldBe(enabled).click();
    }

    @Step
    public void sendKeysToElement(SelenideElement element, String text) {
        element.sendKeys(text);
    }

    @Step
    public void elementShouldHaveText(SelenideElement element, String text) {
        element.shouldHave(text(text));
    }

    @Step
    public void elementShouldHaveLink(SelenideElement element, String href) {
        element.shouldHave(href(href));
    }

    @Step
    public void elementAttributeShouldHaveText(SelenideElement element, String attribute, String text) {
        element.shouldHave(attribute(attribute, text));
    }

    @Step
    public void elementShouldBeVisible(SelenideElement element) {
        element.shouldBe(visible);
    }

    @Step
    public void listShouldHaveTexts(ElementsCollection collection, String... listTexts) {
        collection.shouldHave(texts(listTexts));
    }

    @Step
    public void elementShouldBeVisibleAndHaveText(SelenideElement element, String text) {
        elementShouldBeVisible(element);
        elementShouldHaveText(element, text);

    }

    @Step
    public void pageShouldHaveURL(String url) {
        webdriver().shouldHave(url(url));
    }
}
