package steps;

import io.qameta.allure.Step;
import pages.MainPage;

import static fixtures.Constants.BASE_URL;
import static modals.SignUpModal.*;
import static pages.EmailConfirmPage.*;

public class SignUpSteps extends BaseSteps {

    @Step
    public void openPage() {
        openPage(BASE_URL);
    }

    @Step
    public void clickSignUpButton() {
        elementShouldBeVisible(MainPage.SIGN_UP_BUTTON);
        elementShouldHaveText(MainPage.SIGN_UP_BUTTON, "Sign up free");
        clickOnElement(MainPage.SIGN_UP_BUTTON);
    }

    @Step
    public void clickSignInButton() {
        elementShouldBeVisible(SIGN_UP_SING_IN_BUTTON);
        elementShouldHaveText(SIGN_UP_SING_IN_BUTTON, "Sign in");
        clickOnElement(SIGN_UP_SING_IN_BUTTON);
    }

    @Step
    public void clickGetStartedNowButton() {
        clickOnElement(SIGN_UP_SUBMIT_BUTTON);
    }

    @Step
    public void assertSuccessfullySingUpWindowOpen() {
        elementShouldBeVisibleAndHaveText(SIGN_UP_SING_IN_BUTTON, "Sign in");
        elementShouldBeVisibleAndHaveText(SIGN_UP_TITLE_TEXT, "Get started free today");
        elementShouldBeVisibleAndHaveText(SIGN_UP_SMALL_TITLE_TEXT, "No credit card required");
        elementShouldBeVisibleAndHaveText(SIGN_UP_SMALL_TITLE_TEXT, "No credit card required");
        elementShouldBeVisible(SIGN_UP_NAME_FIELD);
        elementAttributeShouldHaveText(SIGN_UP_NAME_FIELD, "placeholder", "Name");
        elementShouldBeVisible(SIGN_UP_WORK_EMAIL_FIELD);
        elementAttributeShouldHaveText(SIGN_UP_WORK_EMAIL_FIELD, "placeholder", "Work email");
        elementShouldBeVisible(SIGN_UP_PASSWORD_FIELD);
        elementAttributeShouldHaveText(SIGN_UP_PASSWORD_FIELD, "placeholder", "Password 8+ characters");
        elementShouldBeVisible(SIGN_UP_TERMS_CHECKBOX);
        elementShouldBeVisibleAndHaveText(SIGN_UP_TERMS_CHECKBOX_TEXT, "I agree to Miro Terms and Privacy Policy.");
        elementShouldHaveLink(SIGN_UP_TERMS_LINK, "/legal/terms-of-service/");
        elementShouldBeVisible(SIGN_UP_SUBSCRIBE_CHECKBOX);
        elementShouldBeVisibleAndHaveText(SIGN_UP_SUBSCRIBE_CHECKBOX_TEXT, "I agree to receive Miro news and updates.");
        elementShouldHaveLink(SIGN_UP_PRIVATE_POLICY_LINK, "/legal/privacy-policy/");
        elementShouldBeVisibleAndHaveText(SIGN_UP_SUBMIT_BUTTON, "Get started now");
        elementShouldBeVisibleAndHaveText(SIGN_UP_SEPARATOR_TEXT, "or sign up with:");
        elementShouldBeVisible(SIGN_UP_WITH_GOOGLE_BUTTON);
        elementShouldBeVisible(SIGN_UP_WITH_SLACK);
        elementShouldBeVisible(SIGN_UP_WITH_OFFICE365_BUTTON);
        elementShouldBeVisible(SIGN_UP_WITH_APPLE_BUTTON);
        elementShouldBeVisible(SIGN_UP_WITH_FACEBOOK_BUTTON);
        listShouldHaveTexts(SIGN_UP_BENEFIT_LIST,
                "Included for free:", "3 editable boards", "Core integrations", "Unlimited team members",
                "Templates", "Basic attention management");
    }

    @Step
    public void assertErrorMassages() {
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_ENTER_NAME, "Please enter your name.");
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_ENTER_EMAIL, "Enter your email address.");
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_ENTER_PASSWORD, "Enter your password.");
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_AGREE_POLICY, "Please agree with the Terms to sign up.");
    }

    @Step
    public void fillSignUpForm(String name, String email, String password) {
        sendKeysToElement(SIGN_UP_NAME_FIELD, name);
        sendKeysToElement(SIGN_UP_WORK_EMAIL_FIELD, email);
        sendKeysToElement(SIGN_UP_PASSWORD_FIELD, password);
        clickOnElement(SIGN_UP_TERMS_CHECKBOX);
        clickOnElement(SIGN_UP_SUBSCRIBE_CHECKBOX);
    }

    @Step
    public void assertInvalidEmailError() {
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_ENTER_EMAIL, "Enter a valid email address.");
    }

    @Step
    public void assertPasswordPolicyFieldHint(String text) {
        elementShouldBeVisibleAndHaveText(ERROR_SIGN_UP_PASSWORD_HINT_TEXT, text);
    }

    @Step
    public void assertSuccessfullyEmailConfirmPageOpen(String email) {
        elementShouldBeVisible(EMAIL_CONFIRM_MIRO_LOGO);
        elementShouldBeVisibleAndHaveText(EMAIL_CONFIRM_TITLE_TEXT, "Check your email");
        elementShouldBeVisibleAndHaveText(EMAIL_CONFIRM_SUBTITLE_TEXT, "We've sent you a six-digit confirmation " +
                "code to " + email + ". Please enter it below to confirm your email address.");
        elementShouldBeVisible(EMAIL_CONFIRM_CODE_FIELD);
        elementShouldBeVisibleAndHaveText(EMAIL_CONFIRM_CODE_FIELD_PlACEHOLDER, "Enter 6-digit code");
        elementShouldBeVisibleAndHaveText(EMAIL_CONFIRM_FOOTER_TEXT, "Send code again or find more information in Help Center.");
        elementShouldHaveLink(EMAIL_CONFIRM_RESENT_CODE_LINK, "/get-new-code/");
        elementShouldHaveLink(EMAIL_CONFIRM_HELP_CENTER_LINK,
                "https://help.miro.com/hc/en-us/articles/360017731373-Registration-Confirmation-Issues");
        pageShouldHaveURL(BASE_URL + EMAIL_CONFIRM_URL);
    }
}
