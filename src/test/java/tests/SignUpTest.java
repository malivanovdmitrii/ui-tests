package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pages.SignInPage;
import steps.SignUpSteps;

import java.util.UUID;

import static fixtures.Constants.BASE_URL;

@Feature("Sign Up UI tests")
public class SignUpTest extends BaseTest {

    private SignUpSteps signUpSteps = new SignUpSteps();

    @BeforeEach
    public void before() {
        signUpSteps.openPage();
    }

    @Story("Click Sign up free on main page and validate sign up form")
    @Test
    public void test01_checkSignUpModal() {
        signUpSteps.clickSignUpButton();
        signUpSteps.assertSuccessfullySingUpWindowOpen();
    }

    @Story("Confirm empty sign up form")
    @Test
    public void test02_confirmEmptyForm() {
        signUpSteps.clickSignUpButton();
        signUpSteps.clickGetStartedNowButton();
        signUpSteps.assertErrorMassages();
    }

    @Story("Enter invalid email and get error")
    @Test
    public void test03_getInvalidEmailError() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "Qwerty123!");
        signUpSteps.clickGetStartedNowButton();
        signUpSteps.assertInvalidEmailError();
    }

    @Story("Enter short password and get hint")
    @Test
    public void test04_fillShortPassword() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "a");
        signUpSteps.assertPasswordPolicyFieldHint("Please use 8+ characters for secure password.");
    }

    @Story("Enter so-so password and get hint")
    @Test
    public void test04_fillSoSoPassword() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "Qwertyy1yy");
        signUpSteps.assertPasswordPolicyFieldHint("So-so password");
    }

    @Story("Enter weak password and get hint")
    @Test
    public void test05_fillWeakPassword() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "Qwerty12");
        signUpSteps.assertPasswordPolicyFieldHint("Weak password");
    }

    @Story("Enter good password and get hint")
    @Test
    public void test06_fillGoodPassword() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "Qwerty1212!!!");
        signUpSteps.assertPasswordPolicyFieldHint("Good password");
    }

    @Story("Enter great password and get hint")
    @Test
    public void test06_fillGreatPassword() {
        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", "email", "-c!z>zry4/4X)-B");
        signUpSteps.assertPasswordPolicyFieldHint("Great password");
    }

    @Story("Click sign in and check correctly redirect")
    @Test
    public void test07_clickSignIn() {
        signUpSteps.clickSignUpButton();
        signUpSteps.clickSignInButton();
        signUpSteps.elementShouldHaveText(SignInPage.SIGN_IN_TITLE_TEXT, "Sign in");
        signUpSteps.pageShouldHaveURL(BASE_URL + SignInPage.SIGN_IN_PAGE_URL);
    }

    @Story("Successfully sign up")
    @Test
    public void test8_successfullyRegistration() {
        String email = UUID.randomUUID() + "@mail.ru";

        signUpSteps.clickSignUpButton();
        signUpSteps.fillSignUpForm("TestQA", email, "-c!z>zry4/4X)-B");
        signUpSteps.clickGetStartedNowButton();
        signUpSteps.assertSuccessfullyEmailConfirmPageOpen(email);
    }
}
