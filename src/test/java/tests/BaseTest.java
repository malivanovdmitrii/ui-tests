package tests;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import utils.Allure;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static utils.HTTP.clearCookies;

@ExtendWith(Allure.class)
@Testcontainers
public class BaseTest {

    @Container
    static BrowserWebDriverContainer<?> webDriverContainer = new BrowserWebDriverContainer<>()
            .withSharedMemorySize(1000000000L)//1Gb
            .withImagePullPolicy(PullPolicy.ageBased(Duration.ofDays(1)))
            .withRecordingMode(BrowserWebDriverContainer.VncRecordingMode.SKIP, null);


    static {
        Configuration.headless = true;
    }

    @BeforeAll
    static void setUp() {
        Configuration.remote = webDriverContainer.getSeleniumAddress().toString();
    }

    @BeforeEach
    public void beforeBase() {
        clearCookies();
    }

    @AfterEach
    public void afterBase() {
        String script = "console.clear();";
        executeJavaScript(script);
    }
}
