package modals;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SignUpModal {

    public static final SelenideElement

            SIGN_UP_SING_IN_BUTTON = $(".overlay-signup__btn"),
            SIGN_UP_TITLE_TEXT = $(By.xpath("//*[contains(@class, 'signup__title-form signup__title-form--sign-up ab-signup-usa--title')]")),
            SIGN_UP_SMALL_TITLE_TEXT = $(".ab-signup-usa--free-text"),
            SIGN_UP_NAME_FIELD = $(By.id("name")),
            SIGN_UP_WORK_EMAIL_FIELD = $(By.id("email")),
            SIGN_UP_PASSWORD_FIELD = $(By.id("password")),
            SIGN_UP_TERMS_CHECKBOX = $(By.xpath("//*[@data-testid='mr-form-signup-terms-1']//label")),
            SIGN_UP_TERMS_CHECKBOX_TEXT = $(By.id("signup-error-emptyTerms")),
            SIGN_UP_TERMS_LINK = $(By.xpath("//*[@data-testid='mr-link-terms-1']")),
            SIGN_UP_SUBSCRIBE_CHECKBOX = $(By.xpath("//*[@class=\"mr-checkbox-1__check\"][@for=\"signup-subscribe\"]")),
            SIGN_UP_SUBSCRIBE_CHECKBOX_TEXT = $(By.id("signup-subscribe-desc")),
            SIGN_UP_PRIVATE_POLICY_LINK = $(By.xpath("//*[@data-testid='mr-link-privacy-1']")),
            SIGN_UP_SUBMIT_BUTTON = $(".signup__submit"),
            SIGN_UP_SEPARATOR_TEXT = $(By.xpath("//*[contains(@class, 'signup__separator ab-signup-usa--social-text')]")),
            SIGN_UP_WITH_GOOGLE_BUTTON = $(By.id("a11y-signup-with-google")),
            SIGN_UP_WITH_SLACK = $(By.id("kmq-slack-button")),
            SIGN_UP_WITH_OFFICE365_BUTTON = $(By.id("kmq-office365-button")),
            SIGN_UP_WITH_APPLE_BUTTON = $(By.id("apple-auth")),
            SIGN_UP_WITH_FACEBOOK_BUTTON = $(By.xpath("//*[contains(@class, 'signup__btn signup__social__btn signup__btn--facebook')]")),


            ERROR_SIGN_UP_ENTER_NAME = $(By.id("nameError")),
            ERROR_SIGN_UP_ENTER_EMAIL = $(By.id("emailError")),
            ERROR_SIGN_UP_ENTER_PASSWORD = $(By.xpath("//*[@data-testid='please-enter-your-password-1']")),
            ERROR_SIGN_UP_PASSWORD_HINT_TEXT = $(".signup__input-hint-text"),
            ERROR_SIGN_UP_AGREE_POLICY = $(By.id("termsError")
            );



    public static final ElementsCollection
            SIGN_UP_BENEFIT_LIST = $$(".cxl-benefit");

}
