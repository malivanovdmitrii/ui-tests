package utils;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static fixtures.Constants.BASE_URL;

public class HTTP {

    public static void clearCookies() {
        open(BASE_URL);
        if (getWebDriver().manage().getCookies() != null) {
            getWebDriver().manage().deleteAllCookies();
        }
    }
}
