package fixtures;

import static java.lang.System.getenv;

public class Constants {

    public static String
            BASE_URL = getenv("APP_URL") != null ? getenv("APP_URL") : "https://miro.com";
}
