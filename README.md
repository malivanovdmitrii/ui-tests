* **Install and run Docker. Make docker login to dockerhub if you need.**
* **Gradle 6.9**
* **Java 11**
* **Clone project**
* **Open in IntelliJ IDEA and wait while gradle doesn't stop configure project.**

### Run in terminal

* Linux `./gradlew clean test allureReport`
* Windows `gradlew clean test allureReport`

* Generated Allure report `/build/reports/allure-report`
